import React from 'react';
import { Switch, Route, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import EmployeesList from './employeesList.jsx';
import Employee from './employee.jsx';
import EmployeePreview from './employeePreview.jsx';

import './app.less';

class App extends React.Component {
    constructor(props) {
        super(props)                
    }
    render() {
        return (
            <div className="app">

                <div className="linkPanel">
                    <NavLink to="/" className="link">to home</NavLink>
                    <NavLink to="/employeesList" className="link">to employees list</NavLink>
                </div>

                <div className="content">
                    <Switch>
                        <Route exact path="/" render={() => <div>choose the action</div>} />
                        <Route exact path="/employeesList" component={EmployeesList} />
                        <Route path="/employeesList/:employeeId" component={EmployeePreview} />
                        <Route render={() => <div>error 404 not found</div>} />
                    </Switch>
                </div>
            </div>
        )
    }
}
export default App;



