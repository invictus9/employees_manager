import React from 'react';
import PropTypes from 'prop-types';

import './controlPanel.less';


class ControlPanel extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let roles = this.props.getRoles();   
        return (
            <div className="controlPanel">
                <div >
                    Sort by birth
                    <button 
                        className="buttonSort"
                        onClick={this.props.sortByBirth}
                        >
                        Down
                    </button>
                </div>
                <div >
                    Sort by alphabet
                    <button className="buttonSort" onClick={this.props.sortByAlphabet}>
                        A...Z
                    </button>
                </div>
                <div>
                    Sort by role
                    <select onClick={this.props.sortByRoles} className="buttonSort">
                        <option  value="all">
                            all
                        </option>
                        {
                            roles.map((role) => {
                                return <option
                                    className="buttonSort"                                    
                                    key={role}
                                    value={role}
                                >
                                    {role}
                                </option>
                            })
                        }
                    </select>
                </div>
                <div>
                    Sort by archive:
                    <input type="checkbox" 
                        onChange={this.props.sortByArchive}                        
                    />
                    in archive
                </div>
            </div>
        )
    }
}

export default ControlPanel;

ControlPanel.propTypes = {
    sortByAlphabet: PropTypes.func.isRequired,
    getRoles: PropTypes.func.isRequired,
    sortByRoles: PropTypes.func.isRequired,
    sortByBirth: PropTypes.func.isRequired,
    sortByArchive: PropTypes.func.isRequired,
}