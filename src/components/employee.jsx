import React from 'react';
import './employee.less';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

class Employee extends React.Component {
    constructor(props) {
        super(props)

    }

    render() {
        return (
            <div className="employee" >
                <div>{this.props.employee.name}</div>
                <div>{this.props.employee.role}</div>
                <div>{this.props.employee.phone}</div>
                <NavLink to={`/employeesList/${this.props.employee.id}`}>to profile</NavLink>
            </div>
        )
    }
}

export default Employee;

Employee.propTypes = {
    
    employee: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        isArchive: PropTypes.bool,
        role: PropTypes.string,
        phone: PropTypes.string,
        birthday: PropTypes.string
    }).isRequired
}