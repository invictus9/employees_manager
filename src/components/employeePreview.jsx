import React from 'react';
import './employeePreview.less';
import { NavLink } from 'react-router-dom';

import employees from '../../employees/employees.json';

class EmployeePreview extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            "name": "",
            "isArchive": "",
            "role": "",
            "phone": "",
            "birthday": ""
        }

        this.handleEditClick = () => {
            let inputs = document.getElementsByClassName("inputsProfile");
            for (let i = 0; i < inputs.length; ++i){      
                inputs[i].disabled = false;
            }
            document.getElementById("saveChanges").classList.remove("hidden");
        }

        this.handleChangeInput = (event) => {
            switch (event.target.id) {
                case "name":
                    this.setState({
                        name: event.target.value
                    })
                    break;
                case "isArchive":
                    this.setState({
                        isArchive: event.target.value
                    })
                    break;
                case "role":
                    this.setState({
                        role: event.target.value
                    })
                    break;
                case "phone":
                    this.setState({
                        phone: event.target.value
                    })
                    break;
                case "birthday":
                    this.setState({
                        birthday: event.target.value
                    })
                    break;
            
                default:
                    alert("unknown property of employee: " + changesField)
                    break;
            }
        }

        this.getEmployee = (employee_id) => {
            for (let i = 0; i < employees.length; i++) {
                if (employees[i].id == employee_id) {
                    return employees[i]
                }
            }
            return 0;
        }

        this.getRoles = () => {
            let roles = {};
            employees.forEach((employee) => {
                roles[employee.role] = true;                
            });
            var result_roles = [];
            for(var prop in roles){
                result_roles.push(prop)
            }
            return result_roles;    
        }

        this.saveChanges = () => {
            for(let i = 0; i < employees.length; ++i) {
                if (employees[i].id == this.props.match.params.employeeId) {

                    employees[i].name = this.state.name;
                    employees[i].isArchive = this.state.isArchive;
                    employees[i].role = this.state.role;
                    employees[i].phone = this.state.phone;
                    employees[i].birthday = this.state.birthday;
                    
                    break;
                }
            }
            let inputs = document.getElementsByClassName("inputsProfile");
            for (let i = 0; i < inputs.length; ++i){      
                inputs[i].disabled = true;
            }
            document.getElementById("saveChanges").classList.add("hidden");
        }
        

    }

    componentWillMount() {
        var employee = this.getEmployee(this.props.match.params.employeeId);
        this.setState({
            "name": employee.name,
            "isArchive": employee.isArchive,
            "role": employee.role,
            "phone": employee.phone,
            "birthday": employee.birthday
        })
    }

    render() {
        console.log("render employeePreview");

        let roles = this.getRoles();
        

        return (
            <div className="employeePreview">
                <NavLink to="/employeesList">Back to list</NavLink>
                <button onClick={this.handleEditClick}>Edit</button>

                <input className="inputsProfile" 
                    id="name" type="text" 
                    onChange={this.handleChangeInput} 
                    disabled="disabled" 
                    value={this.state.name} 
                />

                <input className="inputsProfile" 
                    id="phone" type="text" 
                    onChange={this.handleChangeInput} 
                    disabled="disabled" 
                    value={this.state.phone}
                />                     

                <input className="inputsProfile" 
                    id="birthday" type="text" 
                    onChange={this.handleChangeInput} 
                    disabled="disabled" 
                    value={this.state.birthday} 
                />     
                
                <select className="inputsProfile" id="role" onChange={this.handleChangeInput} disabled="disabled">
                    <option value={this.state.role} >
                        {this.state.role}
                    </option>
                    {
                        roles.map( (role) => {
                            return (
                                <option  
                                    key={role}
                                    value={role}>
                                    {role}
                                </option>
                            )
                        })
                    }
                </select>

                <select className="inputsProfile" id="isArchive" onChange={this.handleChangeInput} disabled="disabled">
                    <option value={this.state.isArchive} >
                        {this.state.isArchive? "inArchive" : "not inArchive" }
                    </option>
                    <option value={true} >
                        inArchive
                    </option>
                    <option value={false} >
                        not inArchive
                    </option>                    
                </select>

                <button id="saveChanges" className="inputsProfile hidden" onClick={this.saveChanges}   >
                    Save changes
                </button>
                      
            </div>
        )
    }
}

export default EmployeePreview;

 