import React from 'react';
import './employeesList.less';

import ControlPanel from './controlPanel.jsx';
import Employee from "./employee.jsx";
import employees from '../../employees/employees.json';

export default class EmployeesList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            employees: employees,
            filterEmployees: employees,
        }

        this.sortByAlphabet = (event) => {   
            console.log("sortByAlphabet")         
            var employyesSortByAlphabet = this.state.filterEmployees;
            if (event.target.value == "A...Z") {
                employyesSortByAlphabet.sort((employee_previous, employee_next) => {
                    return employee_previous.name < employee_next.name;
                });
                event.target.value = "Z...A";
            } else {
                employyesSortByAlphabet.sort( (employee_previous, employee_next) => {
                    return employee_previous.name > employee_next.name;
                });
                event.target.value = "A...Z";
            }

            this.setState({
                filterEmployees: employyesSortByAlphabet
            })
        }

        this.sortByBirth = (event) => {
            console.log("sortByBirth")
            var employyesSortByBirth = this.state.filterEmployees;
            if (event.target.value == "Down") {
                employyesSortByBirth.sort((employee_previous, employee_next) => {
                    return employee_previous.birthday < employee_next.birthday;
                });
                event.target.value = "Up";
            } else {
                employyesSortByBirth.sort( (employee_previous, employee_next) => {
                    return employee_previous.birthday > employee_next.birthday;
                });
                event.target.value = "Down";
            }

            this.setState({
                filterEmployees: employyesSortByBirth
            })
        }

        this.getRoles = () => {
            let roles = {};
            this.state.employees.forEach((employee) => {
                roles[employee.role] = true;                
            });
            var result_roles = [];
            for(var prop in roles){
                result_roles.push(prop)
            }
            return result_roles;    
        }

        this.sortByRoles = (event) => {
            if(event.target.value === "all"){
                this.setState({
                    filterEmployees: employees
                })
            } else {
                let filterEmployeesByRole = this.state.filterEmployees.filter((employee) => {
                    return event.target.value == employee.role
                })
                this.setState({
                    filterEmployees: filterEmployeesByRole
                })
            }
            console.log("sortByRoles:")
        }

        this.sortByArchive = (event) => {
            let filterEmployeesByRole = this.state.employees.filter((employee) => {
                return event.target.checked == employee.isArchive
            })
            this.setState({
                filterEmployees: filterEmployeesByRole
            })
            
            console.log("sortByArchive:" )
        }
    }

    render(){
        
        console.log("render employeeList");
        return(
            <div >
                <ControlPanel
                    sortByAlphabet={this.sortByAlphabet}
                    getRoles={this.getRoles}
                    sortByRoles={this.sortByRoles}
                    sortByBirth={this.sortByBirth}
                    sortByArchive={this.sortByArchive}
                />

                <div className="employeesList">
                    {
                        this.state.filterEmployees.map((employee) => {
                            return (
                                <Employee
                                    key={employee.id}
                                    employee={employee}
                                />
                            )
                        })
                    }
                </div>
            </div>
        );
    }
} 


