import ReactDOM from 'react-dom';
import React from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import App from './components/app.jsx';
import './main.less';
// import configureStore from './store/configureStore';

// const store = configureStore();

ReactDOM.render(
    <Router>
        <App />
    </Router>,
    document.getElementById("app")
)
