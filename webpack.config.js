var path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
var	webpack	=	require('webpack');

module.exports = {
   entry: "./src/main.jsx", 
   output:{
       path: path.resolve(__dirname, './public'),   
       filename: "bundle.js"     
   },
   devServer: {
     historyApiFallback: true,
   },
   module:{
    loaders: [
      {
        test: /\.jsx$/,
        exclude: [/(node_modules)/, /public/],
        loader:   "babel-loader" 
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: "css-loader!autoprefixer-loader"
        }),
        exclude: [/node_modules/, /public/]
      },
      {
        test: /\.less$/,
        loader: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: "css-loader!autoprefixer-loader!less-loader"
        }),
        exclude: [/node_modules/, /public/]
      },
      {
        test: /\.json$/,
        loader: "json-loader"
      }
    ]
   },
    plugins: [
      new ExtractTextPlugin("style.css")
    ]
}
